﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class Champion
    {
        public bool active { get; set; }
        public string alias { get; set; }
        public string banVoPath { get; set; }
        public bool botEnabled { get; set; }
        public string chooseVoPath { get; set; }
        public string[] disabledQueues { get; set; }
        public bool freeToPlay { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public Ownership ownership { get; set; }
        public Passive passive { get; set; }
        public long purchased { get; set; }
        public bool rankedPlayEnabled { get; set; }
        public string[] roles { get; set; }
        public Skin[] skins { get; set; }
        public Spell[] spells { get; set; }
        public string squarePortraitPath { get; set; }
        public string stingerSfxPath { get; set; }
    }

    public class Ownership
    {
        public bool freeToPlayReward { get; set; }
        public bool owned { get; set; }
        public Rental rental { get; set; }
    }

    public class Rental
    {
        public int endDate { get; set; }
        public long purchaseDate { get; set; }
        public bool rented { get; set; }
        public int winCountRemaining { get; set; }
    }

    public class Passive
    {
        public string description { get; set; }
        public string name { get; set; }
    }

    public class Skin
    {
        public string cardPath { get; set; }
        public int championId { get; set; }
        public string chromaPath { get; set; }
        public Chroma[] chromas { get; set; }
        public bool disabled { get; set; }
        public object[] emblems { get; set; }
        public string featuresText { get; set; }
        public int id { get; set; }
        public bool lastSelected { get; set; }
        public string name { get; set; }
        public Ownership1 ownership { get; set; }
        public string rarityGemPath { get; set; }
        public string skinType { get; set; }
        public string splashPath { get; set; }
        public string splashVideoPath { get; set; }
        public bool stillObtainable { get; set; }
        public string tilePath { get; set; }
        public string loadScreenPath { get; set; }
        public string uncenteredSplashPath { get; set; }
    }

    public class Ownership1
    {
        public bool freeToPlayReward { get; set; }
        public bool owned { get; set; }
        public Rental1 rental { get; set; }
    }

    public class Rental1
    {
        public int endDate { get; set; }
        public long purchaseDate { get; set; }
        public bool rented { get; set; }
        public int winCountRemaining { get; set; }
    }

    public class Chroma
    {
        public int championId { get; set; }
        public string chromaPath { get; set; }
        public string[] colors { get; set; }
        public bool disabled { get; set; }
        public int id { get; set; }
        public bool lastSelected { get; set; }
        public string name { get; set; }
        public Ownership2 ownership { get; set; }
        public bool stillObtainable { get; set; }
    }

    public class Ownership2
    {
        public bool freeToPlayReward { get; set; }
        public bool owned { get; set; }
        public Rental2 rental { get; set; }
    }

    public class Rental2
    {
        public long endDate { get; set; }
        public long purchaseDate { get; set; }
        public bool rented { get; set; }
        public int winCountRemaining { get; set; }
    }

    public class Spell
    {
        public string description { get; set; }
        public string name { get; set; }
    }
}
