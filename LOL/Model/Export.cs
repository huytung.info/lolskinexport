﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{
    public class Export
    {
        public UserInfo userInfo { get; set; }
        public Rank rankedData { get; set; }
        public List<Mastery> masteries { get; set; }
        public List<SkinExport> skins { get; set; }
        public List<Champ> champs { get; set; }
        public string authToken { get; set; }
    }

    public class SkinExport
    {
        public string id { get; set; }
        public string name { get; set; }
        public string idchamp { get; set; }
        public string loadScreenPath { get; set; }
        public string image { get; set; }
    }

    public class Champ
    {
        public string id { get; set; }
        public string name { get; set; }
        public string alias { get; set; }
        public string avatar { get; set; }
        
    }
       
}
