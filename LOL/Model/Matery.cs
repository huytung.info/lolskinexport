﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{

    public class Masteries
    {
        public List<Mastery> masteries { get; set; }
        public long score { get; set; }
        public long summonerId { get; set; }
    }

    public class Mastery
    {
        public long championId { get; set; }
        public int championLevel { get; set; }
        public int championPoints { get; set; }
        public int championPointsSinceLastLevel { get; set; }
        public int championPointsUntilNextLevel { get; set; }
        public bool chestGranted { get; set; }
        public string highestGrade { get; set; }
        public long lastPlayTime { get; set; }
        public long playerId { get; set; }
    }

}
