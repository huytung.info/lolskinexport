﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LOL
{


    public class Rank
    {
        public string highestPreviousSeasonAchievedRank { get; set; }
        public string highestPreviousSeasonAchievedTier { get; set; }
        public string highestPreviousSeasonEndRank { get; set; }
        public string highestPreviousSeasonEndTier { get; set; }
        public List<Queue> queues { get; set; }
    }


    public class Queue
    {
        public int leaguePoints { get; set; }
        public int losses { get; set; }
        public string miniSeriesProgress { get; set; }
        public string previousSeasonAchievedRank { get; set; }
        public string previousSeasonAchievedTier { get; set; }
        public string previousSeasonEndRank { get; set; }
        public string previousSeasonEndTier { get; set; }
        public int provisionalGameThreshold { get; set; }
        public int provisionalGamesRemaining { get; set; }
        public string queueType { get; set; }
        public string rank { get; set; }
        public string tier { get; set; }
        public object warnings { get; set; }
        public int wins { get; set; }
    }


}
